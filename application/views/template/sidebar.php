	<!-- Main Sidebar Container -->
	<aside class="main-sidebar sidebar-dark-primary elevation-4">
		<!-- Brand Logo -->
		<a href="<?= base_url('') ?>" class="brand-link">
			<img src="<?= base_url(''); ?>/assets/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
			<span class="brand-text font-weight-light">AdminLTE 3</span>
		</a>


		<!-- Sidebar Menu -->
		<nav class="mt-2">
			<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
				<!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
				<li class="nav-item menu-open">
					<a href="<?= base_url('') ?>" class="nav-link">
						<i class="nav-icon fas fa-tachometer-alt"></i>
						<p>
							<strong>Dashboard</strong>
							<i class="right fas fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="<?= base_url('Dashboard/ranaf') ?>" class="nav-link">
								<i class="far fa-circle nav-icon"></i>
								<p><strong>Dashboard Ranaf</strong></p>
							</a>
						</li>
						<li class="nav-item">
							<a href="<?= base_url('Dashboard/rajal') ?>" class="nav-link">
								<i class="far fa-circle nav-icon"></i>
								<p><strong>Dashboard Rajal</strong></p>
							</a>
						</li>
					</ul>
				</li>
				<!-- Upload -->
				<li class="nav-item menu-open">
					<a href="<?= base_url('') ?>" class="nav-link">
						<i class="nav-icon fas fa-tachometer-alt"></i>
						<p>
							<strong>Upload File</strong>
							<i class="right fas fa-angle-left"></i>
						</p>
					</a>
					<!-- <ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="<?= base_url('Dashboard/ranaf') ?>" class="nav-link">
								<i class="far fa-circle nav-icon"></i>
								<p><strong>Dashboard Ranaf</strong></p>
							</a>
						</li>
						<li class="nav-item">
							<a href="<?= base_url('Dashboard/rajal') ?>" class="nav-link">
								<i class="far fa-circle nav-icon"></i>
								<p><strong>Dashboard Rajal</strong></p>
							</a>
						</li>
					</ul> -->
				</li>

			</ul>
		</nav>
		<!-- /.sidebar-menu -->
		</div>
		<!-- /.sidebar -->
	</aside>