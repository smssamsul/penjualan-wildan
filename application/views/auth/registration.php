<body class="hold-transition register-page">
 <div class="register-box">
  <div class="card card-outline card-primary">
   <div class="card-header text-center">
    <a href="<?= base_url('') ?>/assets/index2.html" class="h1"><b>Register</b></a>
   </div>
   <div class="card-body">
    <p class="login-box-msg">Register a new membership</p>

    <form class="user" method="post" action="<?php base_url('auth/registration'); ?>">
     <div class="input-group mb-3">
      <input name="name" type="text" class="form-control" placeholder="Full name">
      <div class="input-group-append">
       <div class="input-group-text">
        <span class="fas fa-user"></span>
       </div>
      </div>
     </div>
     <div class="input-group mb-3">
      <input type="email" name="email" class="form-control" placeholder="Email">
      <div class="input-group-append">
       <div class="input-group-text">
        <span class="fas fa-envelope"></span>
       </div>
      </div>
     </div>
     <div class="input-group mb-3">
      <input name="password1" type="password" class="form-control" placeholder="Password">
      <div class="input-group-append">
       <div class="input-group-text">
        <span class="fas fa-lock"></span>
       </div>
      </div>
     </div>
     <div class="input-group mb-3">
      <input name="password2" type="password" class="form-control" placeholder="Retype password">
      <div class="input-group-append">
       <div class="input-group-text">
        <span class="fas fa-lock"></span>
       </div>
      </div>
     </div>
     <div class="row">
      <!-- /.col -->
      <div class="col-4">
       <button type="submit" class="btn btn-primary btn-block">Register</button>
      </div>
      <!-- /.col -->
     </div>
    </form>

    <a href="<?= base_url('Auth') ?>" class="text-center">I already have a membership</a>
   </div>
   <!-- /.form-box -->
  </div><!-- /.card -->
 </div>
 <!-- /.register-box -->