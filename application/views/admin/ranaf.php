<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Dashboard</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('') ?>">Home</a></li>
                        <li class="breadcrumb-item active">Ranaf</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <!-- /.card -->

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">RAWAT INAP</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>

                                    <tr>
                                        <th>Nama</th>
                                        <!-- <th>Kronologi</th>
                                            <th>SPRI</th>
                                            <th>SEP</th>
                                            <th>Penunjang Lab</th>
                                            <th>Penunjang Radiologi</th>
                                            <th>Penunjang dari Luar</th>
                                            <th>CPPT</th>
                                            <th>Billing</th>
                                            <th>Resume Medis</th>
                                            <th>Laporan Operasi</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    foreach ($listPaisenAktif['response'] as $rajal) :
                                        // $data['date_birth'] = $rajal->date_birth;
                                        // $data['nopeg'] = $rajal->nopeg;
                                        // $data['regpid'] = $rajal->regpid;
                                        // $data['pid'] = $rajal->pid;
                                        // $data['perkiraan_tarif_cbgs'] = $rajal->perkiraan_tarif_cbgs;
                                        // $data['sex'] = $rajal->sex;
                                        // $data['room_location'] = $rajal->room_location;
                                        // $data['room_nr'] = $rajal->room_nr;
                                        // $data['room_applicare_code'] = $rajal->room_applicare_code;
                                        // $data['room_prefix'] = $rajal->room_prefix;
                                        // $data['nama_pasien'] = $rajal->nama_pasien;
                                        // $data['no_reg'] = $rajal->no_reg;
                                        // $data['reg_date'] = $rajal->reg_date;
                                        // $data['umur'] = $rajal->umur;
                                        // $data['namadokter'] = $rajal->namadokter;
                                        // $data['petugas'] = $rajal->petugas;
                                        // $data['penjamin'] = $rajal->penjamin;

                                    ?>
                                        <tr>
                                            <td><?= $rajal['nama_pasien']; ?></td>
                                            <!-- <td>Kronologi</td>
                                        <td>SPRI</td>
                                        <td>SEP</td>
                                        <td>Penunjang Lab</td>
                                        <td>Penunjang Radiologi</td>
                                        <td>Penunjang dari Luar</td>
                                        <td>CPPT</td>
                                        <td>Billing</td>
                                        <td>Resume Medis</td>
                                        <td>Laporan Operasi</td> -->
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Nama</th>
                                        <!-- <th>Kronologi</th>
                                        <th>SPRI</th>
                                        <th>SEP</th>
                                        <th>Penunjang Lab</th>
                                        <th>Penunjang Radiologi</th>
                                        <th>Penunjang dari Luar</th>
                                        <th>CPPT</th>
                                        <th>Billing</th>
                                        <th>Resume Medis</th>
                                        <th>Laporan Operasi</th> -->
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->