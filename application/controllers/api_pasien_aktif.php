<?php date_default_timezone_set("Asia/Jakarta"); ?>
<!DOCTYPE html>
<html>
	<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<!-- <script type="text/javascript">
	$(document).ready(function() {
  
  pageScroll();
  $("#contain").mouseover(function() {
    clearTimeout(my_time);
  }).mouseout(function() {
    pageScroll();
  });
  
  getWidthHeader('table_fixed','table_scroll');
  
});

var my_time;
function pageScroll() {
	var objDiv = document.getElementById("contain");
  objDiv.scrollTop = objDiv.scrollTop + 1;  
  if ((objDiv.scrollTop + 1000) == objDiv.scrollHeight) {//nilai 1000 buat auto ke atas
    objDiv.scrollTop = 0;
  }
  my_time = setTimeout('pageScroll()', 25);
}

function getWidthHeader(id_header, id_scroll) {
  var colCount = 0;
  $('#' + id_scroll + ' tr:nth-child(1) td').each(function () {
    if ($(this).attr('colspan')) {
      colCount += +$(this).attr('colspan');
    } else {
      colCount++;
    }
  });
  
  for(var i = 1; i <= colCount; i++) {
    var th_width = $('#' + id_scroll + ' > tbody > tr:first-child > td:nth-child(' + i + ')').width();
    $('#' + id_header + ' > thead th:nth-child(' + i + ')').css('width',th_width + 'px');
  }
}



</script> -->

<style>
	* {
  font-family: 'open sans';
}
#contain {
  height: 1000px;  /*nilai 1000 buat auto ke atas*/
  overflow-y: scroll;  
}
#table_scroll {
  width: 100%;
  margin-top: 100px;
  margin-bottom: 100px;
}
#table_scroll tbody td {
  padding: 10px;
  /*background-color: #7fe55e;*/
  /*color: #fff;*/
}
#table_fixed thead th {
  padding: 10px;
  background-color: #b90be0;
  color: #fff;
  font-weight: 100;
}



</style>

	<style>
	table {
		border-collapse: collapse;
		width: 100%;
	}

	th {
		background-color: #04AA6D;
		color: white;
		text-align: center;
		padding: 5px;
	}

	td {
		padding-left: 10px;
	}

	tr:nth-child(even){background-color: #f2f2f2}
	
	h5 {
		color: #04AA6D;
		text-align: center;
		margin:0px;
	}

	h6 {
		color: red;
		text-align: center;
		margin:0px;
	}
	
	.btn-primary {
		background-color: #4CAF50; /* Green */
		border: none;
		color: white;
		padding: 3px 7px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		border-radius: 10px;
	}
	
	.label1 {
		background-color: #4CAF50; /* Green */
		border: none;
		color: white;
		padding: 2px 5px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		border-radius: 10px;
	}
	
	.label2 {
		background-color: #42cbf5; /* Green */
		border: none;
		color: white;
		padding: 2px 5px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		border-radius: 10px;
	}
	
	.label3 {
		background-color: #f52727; /* Green */
		border: none;
		color: white;
		padding: 2px 5px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		border-radius: 10px;
	}
	
	.label4 {
		background-color: #9c27f5; /* Green */
		border: none;
		color: white;
		padding: 2px 5px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		border-radius: 10px;
	}
	
	.label5 {
		//background-color: #9c27f5; /* Green */
		border: block;
		//color: white;
		padding: 2px 5px;
		text-align: right;
		text-decoration: none;
		display: inline-block;
		border-radius: 10px;
	}
	
	</style>

	</head>
	<body>
	<h5>KETERSEDIAAN TEMPAT TIDUR</h5>	
	<h5>RS UMMI BOGOR</h5>	
	<h6>DATA ONLINE TGL : <?= date("d-m-Y").' JAM : '.date("h:i"); ?></h6>	
	<h5>DETAIL INFORMASI RUANG</h5>	

	<?php
	//$URLnya = "http://125.208.135.161:1820/rsummi-api/BPJShasan/GetRiwayatKunjunganInap1hasan";		
	$URLnya = "http://192.168.5.24/rsummi-api/BPJShasan/GetRiwayatKunjunganInap1hasan";		
	$urut='1';
	$curl = curl_init();
	curl_setopt_array($curl, array(
		CURLOPT_URL => $URLnya,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 60,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
			"authorization: Basic cnN1bW1pOnJzdW1taQ==",
			"cache-control: no-cache"
		),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
		echo "cURL Error #:" . $err;
	} else {
		$someArray = json_decode($response,true);
		
		echo '<div id="contain"> <table id="table_scroll">
			 <tr>
			 <th>NO</th>
				<th>RM</th>	
				<th>TGL LAHIR</th>				
				<th>NAMA</th>				
				<th>MASUK</th>				
				<th>RUANG</th>
				<th>DPJP</th>
				<th>HAK KELAS</th>
				<th>JAMINAN</th>				
				<th>LOS</th>
				<th>DEPOSIT</th>				
				<th>TARIF RS</th>				
				<th>TARIF INACBG</th>
				<TH>SELISIH</TH>					
			</tr>';
			
		$ProsentaseTarifRS 	= 0;
		$ProsentaseTarifCBG = 0;
		
		foreach($someArray["response"] as $keydata => $mydata )
		{


			$tgl1 = new DateTime($mydata["reg_date"]);
			$tgl2 = new DateTime( date('Y-m-d H:m:s'));
			$d = $tgl2->diff($tgl1)->days + 1;
			if (date('d-m-Y', strtotime($mydata["reg_date"])) == date('d-m-Y')) {
				$d = $d + 0;
			} else {
				$d = $d + 1;
			}
						
			if($d > 0){
				$TmpLOS = '<span class="label1">' . $d. '</span>';
			} elseif ($d <= 5) {
				$TmpLOS = '<span class="label2">' . $d. '</span>';
			} else {
				$TmpLOS = '<span class="label3">' . $d. '</span>';
			}
			//echo $d." hari";
			if ($mydata["penjamin"] == 'UMUM') {
				$TmpPenjamin = '<span class="label2">'.$mydata["penjamin"].'</span>';
			}elseif ($mydata["penjamin"] == 'BPJS KESEHATAN') {
				$TmpPenjamin = '<span class="label1">'.$mydata["penjamin"].'</span>';
			}elseif ($mydata["penjamin"] == 'DINAS KESEHATAN KOTA BOGOR') {
				$TmpPenjamin = '<span class="label4">'.$mydata["penjamin"].'</span>';
			}elseif ($mydata["penjamin"] == 'DINAS KESEHATAN') {
				$TmpPenjamin = '<span class="label4">'.$mydata["penjamin"].'</span>';
			} else {
				$TmpPenjamin = '<span class="label3">'.$mydata["penjamin"].'</span>';
			}
			
			//$TmpPenjamin = $TmpPenjamin. '<span class="label5">'.$mydata["room_location"].'</span>';
			
			$TmpBiayaPerawatan = BiayaPerawatan($mydata["regpid"]);
			$TmpBiayaPerawatan = $TmpBiayaPerawatan + ($TmpBiayaPerawatan * 5 / 100);
			
			if ($mydata["penjamin"] == 'BPJS KESEHATAN' || $mydata["penjamin"] == 'PRO BPJS 3') {
				if ($mydata["perkiraan_tarif_cbgs"] <= 0) {
					$TmpBiayaInaCBG = '<span class="label3">Belum di input</span>';
				} else {
					$TmpBiayaInaCBG = 'Rp. '.number_format($mydata["perkiraan_tarif_cbgs"],2,",",".");
				}
				$ProsentaseTarifRS	= $ProsentaseTarifRS + $TmpBiayaPerawatan;
				$ProsentaseTarifCBG	= $ProsentaseTarifCBG + $mydata["perkiraan_tarif_cbgs"];
			}elseif ($mydata["penjamin"] == 'DINAS KESEHATAN KOTA BOGOR') {
				if ($mydata["perkiraan_tarif_cbgs"] <= 0) {
					$TmpBiayaInaCBG = '<span class="label3">Belum di input</span>';
				} else {
					$TmpBiayaInaCBG = 'Rp. '.number_format($mydata["perkiraan_tarif_cbgs"],2,",",".");
				}
				$ProsentaseTarifRS	= $ProsentaseTarifRS + $TmpBiayaPerawatan;
				$ProsentaseTarifCBG	= $ProsentaseTarifCBG + $mydata["perkiraan_tarif_cbgs"];	
			}elseif ($mydata["penjamin"] == 'DINAS KESEHATAN KABUPATEN BOGOR') {
				if ($mydata["perkiraan_tarif_cbgs"] <= 0) {
					$TmpBiayaInaCBG = '<span class="label3">Belum di input</span>';
				} else {
					$TmpBiayaInaCBG = 'Rp. '.number_format($mydata["perkiraan_tarif_cbgs"],2,",",".");
				}
				$ProsentaseTarifRS	= $ProsentaseTarifRS + $TmpBiayaPerawatan;
				$ProsentaseTarifCBG	= $ProsentaseTarifCBG + $mydata["perkiraan_tarif_cbgs"];	
			}elseif ($mydata["penjamin"] == 'DINAS KESEHATAN') {
				if ($mydata["perkiraan_tarif_cbgs"] <= 0) {
					$TmpBiayaInaCBG = '<span class="label3">Belum di input</span>';
				} else {
					$TmpBiayaInaCBG = 'Rp. '.number_format($mydata["perkiraan_tarif_cbgs"],2,",",".");
				}
				$ProsentaseTarifRS	= $ProsentaseTarifRS + $TmpBiayaPerawatan;
				$ProsentaseTarifCBG	= $ProsentaseTarifCBG + $mydata["perkiraan_tarif_cbgs"];
			} else {
				$TmpBiayaInaCBG 	= 'Rp. '.number_format($TmpBiayaPerawatan,2,",",".");
				$ProsentaseTarifRS	= $ProsentaseTarifRS + $TmpBiayaPerawatan;
				$ProsentaseTarifCBG	= $ProsentaseTarifCBG + $TmpBiayaPerawatan;
			}

				if ($d > -1){
							$curl2 = curl_init();
//echo $mydata["nopeg"];
				curl_setopt_array($curl2, array(
				  CURLOPT_URL => 'http://192.168.5.24/PRAS/rsummi.php?param1=CekKepesertaanDenganNoKartu&param2='.$mydata["nopeg"],
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => '',
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 0,
				  CURLOPT_FOLLOWLOCATION => true,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => 'GET',
				));

				$response2 = curl_exec($curl2);
				curl_close($curl2);
				$Posisnya = strpos($response2,'metaData');
						$response3 = substr($response2,$Posisnya-2);
				//echo $response2."<br>";
				$datahakkelas = json_decode($response3,true);
				$selisihbg = $mydata["perkiraan_tarif_cbgs"]-$TmpBiayaPerawatan;			
				$hakKelas = $datahakkelas['response']['peserta']['hakKelas']['kode'];

				echo '<tr><td><b>'.$urut.'</b></td>'.
					 '<td><b>'.$mydata["pid"].'</b></td>'.
					 '<td><b>'.$mydata["date_birth"].'</b></td>'.
					 '<td><b>'.$mydata["nama_pasien"].'</b></td>'.
					 '<td>'.date('d-m-Y', strtotime($mydata["reg_date"])).'</td>'.
					 '<td>'.$mydata["room_prefix"].$mydata["room_nr"].'</td>'.
					 '<td>'.$mydata["namadokter"].'</td>'.
					 '<td>Kelas : '.$hakKelas.'</td>'.
					 '<td><p class="datadetail">'.$TmpPenjamin.'</p></td>'.
					 '<td>'.$TmpLOS.'</td>'.
					 '<td>Rp. '.number_format($mydata["deposit"],2,",",".").'</td>'.
					 '<td>Rp. '.number_format($TmpBiayaPerawatan,2,",",".").'</td>'.
					 '<td>'.$TmpBiayaInaCBG.'</td>'.
					 '<td>Rp. ';
				//
				if ($mydata["penjamin"] == 'UMUM') {
					echo "0";
				}elseif ($mydata["penjamin"] == 'BPJS KESEHATAN') {
					echo number_format($selisihbg,2,",","."); 
				}elseif ($mydata["penjamin"] == 'DINAS KESEHATAN KOTA BOGOR') {
					echo number_format($selisihbg,2,",","."); 
				}elseif ($mydata["penjamin"] == 'DINAS KESEHATAN KABUPATEN BOGOR') {
					echo number_format($selisihbg,2,",","."); 
				}elseif ($mydata["penjamin"] == 'DINAS KESEHATAN') {
					echo number_format($selisihbg,2,",","."); 
				} else {
					echo "0";
				}
				//
				echo '</td>';
			 echo '</tr>';
			 $urut= $urut + 1;
			}
		}
		
		echo '</table></div>';  
            
		echo '<hr><canvas id="myChart1" style="width:100%;max-width:600px"></canvas>';
		
		echo '<script>
				var xValues 	= ["Tarif RS", "Tarif InaCBG"];
				var yValues 	= ['.$ProsentaseTarifRS.','.$ProsentaseTarifCBG.'];
				var barColors 	= ["#008000","#cc0000"];

				new Chart("myChart1", {
				  type: "bar",
				  data: {
					labels: xValues,
					datasets: [{
					  backgroundColor: barColors,
					  data: yValues
					}]
				  },
				  options: {
					legend: {display: false},
					title: {
					  display: true,
					  text: "Perbandingan Tarif RS dan InaCBG"
					}
				  }
				});
				</script>';
				$selisihbg2=$ProsentaseTarifCBG-$ProsentaseTarifRS;
		echo '<center>Tarif RS : Rp. '.number_format($ProsentaseTarifRS,2,",",".").'<br>Tarif InaCBG : Rp. '.number_format($ProsentaseTarifCBG,2,",",".").'<br>Tarif Selisih : Rp. '.number_format($selisihbg2,2,",",".").'</center><hr>';
		echo '<hr><center><a href="api_getbed.php" class="btn-primary">KEMBALI </a></center><br><br>';
		
	}
	
	function BiayaPerawatan($NoReg) {
		/*$URLnya = "http://125.208.135.161:1820/rsummi-api/BPJShasan/GetBiayaPerawatan/$NoReg";*/
		$URLnya = "http://192.168.5.24/rsummi-api/BPJShasan/GetBiayaPerawatan/$NoReg"; 
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $URLnya,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);
		
		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			$someArray = json_decode($response,true);
		}
		
		return $someArray["total"];
	}
?>

</body>
</html>