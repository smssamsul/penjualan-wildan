<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->library('session');
  }

  public function index()

  {
    $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
    $this->form_validation->set_rules('password', 'Password', 'trim|required');

    if ($this->form_validation->run() == false) {
      $this->load->view('auth/header');
      $this->load->view('auth/login');
      $this->load->view('auth/footer');
    } else {
      //validasi succes
      $this->_login();
    }
  }

  private function _login()
  {
    $email = $this->input->post('email');
    $password = $this->input->post('password');

    $user = $this->db->get_where('users', ['email' => $email])->row_array();

    //var_dump($user); 
    //die;
    # usernya ada..
    if ($user) {
      # jisa user aktif..
      if ($user['is_active'] == 1) {
        # cek password
        if (password_verify($password, $user['password'])) {
          # jika benar
          $data = [
            'email' => $user['email'],
            'role_id' => $user['role_id']
          ];

          $datas = $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert"><center>Welcome To Pakegesit</center></div>');
          $datas = $this->session->set_userdata($data);


          if ($user['role_id'] == 1) {
            redirect('Dashboard');
          } else if ($user['role_id'] == 2) {
            redirect('Users');
          } else {
            $this->load->view('auth/header');
            $this->load->view('auth/login');
            $this->load->view('auth/footer');
          }
        } else {
          $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
              wrong password</div>');
          redirect('auth');
        }
      } else {
        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
              this email has not been ctiveted</div>');
        redirect('auth');
      }
    } else {
      $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
        email is not registerd</div>');
      redirect('auth');
    }
  }




  public function registration()
  {
    $this->form_validation->set_rules('name', 'Name', 'required|trim');
    $this->form_validation->set_rules(
      'email',
      'Email',
      'required|trim|valid_email|is_unique[users.email]',
      ['is_unique' => 'This Emil has already registerd!']
    );

    $this->form_validation->set_rules('password1', 'Password', 'required|trim');
    //|
    //min_length[3]|matches[password2]', [
    //'matches' => 'Password dont match!',
    //'min_length' => 'Password too short!'
    //]);

    //$this->form_validation->set_rules('password2','Password','required|trim|
    //matches[password1]');

    if ($this->form_validation->run() == false) {
      $this->load->view('auth/header');
      $this->load->view('auth/registration');
      $this->load->view('auth/footer');
    } else {

      $data = [
        'name' => htmlspecialchars($this->input->post('name', true)),
        'email' => htmlspecialchars($this->input->post('email', true)),
        'image' => 'defaul.jpg',
        'password' => password_hash(
          $this->input->post('password1'),
          PASSWORD_DEFAULT
        ),
        'role_id' => 2,
        'is_active' => 1,
        'date_created' => time()
      ];

      $this->db->insert('users', $data);
      $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
      Congratulation has been Created. please Login</div>');
      redirect('auth');
    }
  }
  public function logout()
  {
    $this->session->unset_userdata('email');
    $this->session->unset_userdata('role_id');
    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
          you have been logged out</div>');
    redirect('auth');
  }
}
