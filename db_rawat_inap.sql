/*
Navicat MySQL Data Transfer

Source Server         : ummi
Source Server Version : 80030
Source Host           : localhost:3306
Source Database       : db_rawat_inap

Target Server Type    : MYSQL
Target Server Version : 80030
File Encoding         : 65001

Date: 2022-10-13 11:07:59
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(100) NOT NULL,
  `image` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(100) NOT NULL,
  `role_id` int NOT NULL,
  `is_active` int NOT NULL,
  `date_created` int NOT NULL,
  `hp_kurir` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('7', 'super admin', 'super.admin@gmail.com', 'default.jpg', '$2y$10$r0nHWNgykkWqq/g5jvT30ulsv3t4pFZZhYkzgznN08ed2q09yUGni', '4', '1', '1645500712', '');
INSERT INTO `users` VALUES ('8', 'Administrator', 'admin@gmail.com', 'default.jpg', '$2y$10$da9GcMfSC87/vh4CC1wySeCT4DWz4Tta3pvXqwrBC5SrW7c88IFGi', '1', '1', '1648800102', '');
INSERT INTO `users` VALUES ('12', 'fadilah yuda pratama', 'fadilahyudapratamarsummi@gmail.com', 'defaul.jpg', '$2y$10$jJ3.JfwArLLmi.2Wc77em.99BVIYdi4Lc.hyfn1NPRQEIX6GxDj6u', '2', '1', '1665631398', '');
